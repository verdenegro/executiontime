<?php
namespace Verdenegro\ExecutionTime;

use Yii;

class ExecutionTime {

    private static $active = false;
    private static $type = 'file';
    private static $file = '';
    private static $db = null;
    private static $dsn = null;
    private static $dbName = null;
    private static $dbUsername = null;
    private static $dbPassword = null;
    private static $queueData = null;

    public static function init($options = []) {
        self::$active = ( isset($options['active']) ) ? $options['active'] : true;
        if( !self::$active ) return;

        // ** Type, DB or File.
        self::$type = ( isset($options['type']) ) ? $options['type'] : 'file';
        self::$type = ( in_array( self::$type, ['file', 'db'] ) ) ? self::$type : 'file';
        // ** Inline or to the end.
        self::$queueData = ( isset($options['queueData']) && $options['queueData'] == true ) ? [] : null;

        $defaultFile = '/tmp/extra.log';
        switch( self::$type ) {
            case 'file':
//                $defaultFile = Yii::getAlias('@runtime').'/ExecutionTime.log';
                self::$file = ( isset($options['file']) ) ? $options['file'] : $defaultFile;
                break;
            case 'db':
                $defaultDbName = 'vn_rTime';
                $defaultDsn = 'sqlite:' . dirname(__DIR__) . '/data/vn_rTime.sqlite';
                self::$dbName = ( isset($options['dbName']) ) ? $options['dbName'] : $defaultDbName;
                self::$dsn = ( isset($options['dsn']) ) ? $options['dsn'] : $defaultDsn;
                self::$dbUsername = ( isset($options['dbUsername']) ) ? $options['dbUsername'] : '';
                self::$dbPassword = ( isset($options['dbPassword']) ) ? $options['dbPassword'] : '';
                if ( self::$queueData === null ) {
                    try {
                        self::$db = new \PDO(self::$dsn, self::$dbUsername, self::$dbPassword);
                    } catch (\Exception $e) {
                        $msg = $e->getMessage();
                    }
                }
                break;
            default:
                self::$type = 'file';
//                self::$file = Yii::getAlias('@runtime').'/ExecutionTime.log';
                self::$file = $defaultFile;
                break;
        }
    }

    public static function rTime( $description = 'inter', $force = false ) {
        if ( !self::$active ) return;

        if ( self::$queueData === null || $force ) {
            if ( self::$type == 'file' ) {
                self::rTimeToFile($description);
            } else if ( self::$type == 'db' ) {
                self::rTimeToDb($description);
            }
        } else {
            self::$queueData[] = [
                'description' => $description,
                'time' => microtime(true)
            ];
        }
    }

    private static function rTimeToFile( $description = 'inter', $time = null ) {
        if ( !$time ) $time = microtime(true);
        $difftime = $time - _INIT_EXECUTION_TIME;

        try {
            file_put_contents(self::$file, "Time: $difftime - ".getmypid()." - $description\n", FILE_APPEND);
        } catch (\Exception $exception) {

        }
    }

    private static function rTimeToDb( $description = 'inter', $time = null ) {
        if ( !$time ) $time = microtime(true);
        $difftime = $time - _INIT_EXECUTION_TIME;

        $db = self::$db;

        $db->beginTransaction();
        try {
            $dnName = self::$dbName;

            $sql = "insert into {$dnName} ('descripcion', 'time', 'difftime', 'created_at') 
                    values (:descripcion, :time, :difftime, :created_at)";
            $statment = $db->prepare($sql);

            $now = date("Y-m-d H:i:s");
            $statment->bindParam(':descripcion', $description, \PDO::PARAM_STR);
            $statment->bindParam(':time', $time, \PDO::PARAM_STR);
            $statment->bindParam(':difftime', $difftime, \PDO::PARAM_INT);
            $statment->bindParam(':created_at', $now, \PDO::PARAM_STR);

            $statment->execute();

            $db->commit();
        } catch (\Exception $exception) {
            $db->rollBack();
        }
    }

}
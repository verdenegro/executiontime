-- Register Execution Time

CREATE TABLE `vn_rTime` (
  `id`          INTEGER NOT NULL  PRIMARY KEY AUTOINCREMENT,  -- PK

  `descripcion` TEXT          NOT NULL,
  `time`        varchar(19)   NOT NULL DEFAULT '0000-00-00 00:00:00',
  `difftime`    INTEGER       NOT NULL,
  `created_at`  varchar(19)   NOT NULL DEFAULT '0000-00-00 00:00:00'
);
